# -*- coding: utf-8 -*-
"""Модуль загрузки данных
Позволяет загружать данные, проводить их анализ, преобразование и оперировать несколькими наборами данных
одновременно, используя лишь высокоуровневые методы с заранее установленными оптимальными параметрами, но
предоставляя возможность гибкой настройки на каждом этапе манипуляции с данными.

"""

import os
import csv
import warnings
import numpy as np
from sklearn.model_selection import StratifiedKFold, train_test_split

__all__ = ['datasets', 'Dataset']

MY_PATH = os.path.dirname(__file__) + '/' if os.path.dirname(__file__) else './'


class Dataset:
    data = []  # X
    targets = []  # Y
    target_names = []  # имена классов

    class trash:  # удаленные образцы
        data = []
        targets = []
        target_names = []

    class meta:
        def clear(self):
            self.balanced, self.samples, self.samples_by_classes, self.capacity, self.capacity_by_classes = [None]*5

        balanced = None
        samples = None
        samples_by_classes = None
        capacity = None
        capacity_by_classes = None

    def __init__(self, **args):
        self.load(**args)

    def analyze(self):
        """
        анализ выборки - заполнение меты
        """
        pass

    def heal(self, bad_samples=True, small_samples=True, target_mistakes=False):
        """
        восстановление данных

        :param bad_samples: удалить образцы без метки класса
        :param small_samples: удалить образцы, текст которых меньше указанного количества символов
        :param target_mistakes: исправить ошибки в названиях классов (одинаковые названия в разных регистрах,
                               небольшие опечатки)
        """
        pass

    def balance(self):
        """
        Балансировка выборки
        """
        pass
        # self.meta.balanced = True

    def get_split(self, test_size=None, train_size=None, randomize=False):
        """
        Создает стратифицированное и рандомизированное разделение загруженных данных

        :param train_size: количество образцов (int) или доля от выборки (float 0-1) для обучения
        :param test_size: количество образцов (int) или доля от выборки (float 0-1) для проверки,
                          если не задано, высчитывается автоматически относительно train_size
        :param randomize: перемешать образцы перед разделением
        :return: разделение вида ([train_data], [test_data], [test_data], [test_targets])
        """
        return train_test_split(self.data, self.targets, random_state=randomize, stratify=self.targets,
                                test_size=test_size, train_size=train_size)

    def get_cross_split(self, parts=3, randomize=False):
        """
        Создает генератор стратифицированных разделений

        :param parts: количество частей, на которые делится начальная выборка
        :param randomize: перемешать ли значения перед разделением
        :return: генератор разделений вида ([train_data], [test_data], [train_targets], [test_targets])
        """
        if parts < 2:  # проверка допустимости значения
            parts = 2
            print('Количество частей при разделении не может быть меньше двух, значение автоматически изменено на 2')
        tmp_indexes = StratifiedKFold(n_splits=parts, shuffle=randomize).split(self.data, self.targets)
        # ловим предупреждения от numpy и не даем сформировать непредсказуемую выборку
        with warnings.catch_warnings(record=True) as warning:
            warnings.simplefilter("always")
            generator = [(np.array(self.data)[train_index].tolist(), np.array(self.data)[test_index].tolist(),
                          np.array(self.targets)[train_index].tolist(), np.array(self.targets)[test_index].tolist())
                         for train_index, test_index in tmp_indexes]
            if warning:
                print('Невозможно выполнить разделение с указанными параметрами. '
                      'Выполните восстановление, балансировку и анализ набора данных, и проверьте количество '
                      'образцов в наименьших классах, возможно их недостаточно для стратификации.')
                return
            return generator

    def load(self, file='default.csv', balance='', analyze=True, heal=True, title=True,
             class_col=1, text_col=2, delimiter=',', quotechar='"', **_):
        """
        Загружает данные по указанным параметрам

        :param file: относительный или полный путь к файлу с исходными данными для построения модели
        :param balance: сбалансировать выборку - сделать во всех категориях приблизительно равное
                        количество образцов одним из возможных способов, задаваемых параметром:
                            excluding - исключить из выборки нерепрезентативные классы, для которых количество образцов
                                        меньше половины от среднего (не рекомендуется использовать, если выборка
                                        должна содержать все возможные классы)
                            undersampling - удалить часть выборки больших классов (может значительно сократить
                                            количество образцов, не рекомендуется применять, если в выборке есть
                                            классы с очень малым количеством образцов)
                            undersampling_x - то же, что и undersampling, но вместо удаления образцы попадают во вторую
                                              часть выборки (так как обычно она используется для оценки, увеличение
                                              количества образцов положительно скажется на оценке). Не влияет на
                                              результат, если не был указан параметр divide с параметром разделения t
                            oversampling - дублировать часть выборки малых классов (может увеличить количество ошибок,
                                           если выборки малых классов нерепрезентативны)
        :param analyze: выполнить анализ выборки после загрузки
        :param heal: исправить поврежденные данные
        :param title: имеет ли таблица заголовок
        :param class_col: столбец с классом образцов
        :param text_col: столбец с текстом образцов
        :param delimiter: символ-разделитель
        :param quotechar: символ-индикатор строковых данных
        """
        text_col += -1
        class_col += -1
        if not os.path.exists(file):
            print('Не удалось найти файл "' + file + '" с базой для обучения. '
                  'Проверьте его наличие и перезапустите обработку')
            return None
        print('Загрузка данных...')
        # собираем инфу о количестве строк и сразу считываем данные
        with open(file, 'r', newline='', encoding="utf-8") as file:
            # загружаем данные в массив
            source = csv.reader(file, delimiter=delimiter, quotechar=quotechar)
            for i, row in enumerate(source):
                if i == 0 and title:  # пропускаем заголовок, если нужно
                    continue
                self.data.append(row[text_col])  # сохраняем данные
                if row[class_col] not in self.target_names:  # сохраняем имя класса, если еще нет
                    self.target_names.append(row[class_col])
                class_alias = self.target_names.index(row[class_col])  # выбираем числовой псевдоним класса
                self.targets.append(class_alias)  # сохраняем метку класса
        print('Загрузка успешно завершена')

        if heal:
            self.heal()

        if balance:
            self.balance()

        if analyze:
            self.analyze()


class Datasets:
    __datasets__ = {}

    def __call__(self, id=''):
        """при вызове datasets('id') возвращает объект набора данных с указанным id"""
        return self.__datasets__.get(id)

    def list(self):
        """выводит массив id всех загруженных наборов данных"""
        return list(self.__datasets__.keys())

    def drop(self, id):
        """удаляет набор данных с указанным id из коллекции"""
        self.__datasets__.pop(id, None)

    def new(self, id, **load_args):
        """
        создает новый объект класса dataset, загружает в него данные из указанного файла и добавлет в коллекцию
        :param id: id под которым сохраняется объект набора данных
        :param load_args: параметры загрузки
        :return: объект только что загруженного набора данных
        """
        self.__datasets__[id] = Dataset(**load_args)
        return self.__datasets__.get(id)


datasets = Datasets()  # управляющий объект для наборов данных всегда один
