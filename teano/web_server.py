# -*- coding: utf-8 -*-
"""Модуль сетевого API
запускает сервер на указанном в настройках порту и предоставляет высокоуровневое API модулей
"""

__all__ = ['start', 'stop', 'reconfigure']


def start():
    pass


def stop():
    pass


def reconfigure():
    pass