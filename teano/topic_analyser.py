# -*- coding: utf-8 -*-
"""Описание модуля


"""
from .data_loader import Dataset
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import cross_val_score
from sklearn.svm import SVC  # svm
from sklearn import metrics
import pickle
import json
from pymystem3 import Mystem
import os
import sys
import logging
import statistics
import re


__all__ = ['models', 'Model']


DEFAULT_ID = 'default'
# полный путь к файлу скрипта
MY_PATH = os.path.dirname(__file__) + '/' if os.path.dirname(__file__) else './'
MODELS_PATH = MY_PATH + 'models/'
lprint = logging.getLogger('TOPIC_ANALYSER')  # логгер модуля


class Model:
    __filter__ = None  # словарь с фильтрами для векторизатора
    __tfidf__ = None  # объект с расчитанной tfidf матрицей
    __predictor__ = None  # обученный классификатор
    __target_names__ = None

    def __init__(self, filter_rules=None, file=None, data=None, **details):
        """
        Загружает или обучает новую модель
        Рассчитывает tfidf матрицу и обучает на ее основе классификатор, сохраняет объект как файл с
        именем %ID%.topic
        """
        # загрузка/формализация переданных данных
        train_data = None
        if data:
            try:
                if type(data) is dict:
                    train_data = {'data': data['data'], 'targets': data['targets']}
                    self.__target_names__ = data['target_names']
                else:
                    train_data = {'data': data.data, 'targets': data.targets}
                    self.__target_names__ = data.target_names
            except Exception:
                print('Неверный формат аргумента data. Необходимо передать словарь '
                      'с ключами data, target, target_names, либо объект с одноименными атрибутами.')
                return
        else:
            if file and os.path.exists(file):
                tmp_dataset = Dataset(file=file, **details)
                train_data = {'data': tmp_dataset.data, 'targets': tmp_dataset.targets}
                self.__target_names__ = tmp_dataset.target_names
                del tmp_dataset
            else:  # если ни данных ни файла не было передано
                print('Невозможно создать модель без данных! '
                      'Укажите верно один из аргументов file или data.')
                return

        # ищем и згружаем фильтры
        if filter_rules:
            if os.path.exists(filter_rules):
                with open(filter_rules) as ff:
                    self.__filter__ = json.load(ff)
                # загрузка фильтра из указанного файла или поиск подходящего
            else:
                print('Файл с правилами фильтрации не существует, либо путь задан неверно, '
                      'проверьте правильность параметра filter_rules.')

        # чистим набор данных перед обучением, если есть фильтры
        if self.__filter__:
            print('Фильтр загружен, выполняется чистка образцов.')
            del_items = []
            samples_count = range(len(train_data['targets']))
            for i in samples_count:
                if not self.__tokenizer__(train_data['data'][i]):
                    del_items.append(i)
                # выводим статус
                sys.stdout.write('\rПроверено {}/{} образцов...'.format(i + 1, len(samples_count)))
                sys.stdout.flush()
            print()
            # пересобираем targets и data
            for i in reversed(del_items):
                del train_data['data'][i]
                del train_data['targets'][i]

        # рассчитываем матрицу tf_idf
        print('Расчет tfidf матрицы...')
        self.__tfidf__ = TfidfVectorizer(input='content', max_df=1.0, min_df=0.0, max_features=50000,
                                         ngram_range=(1, 3),  # stop_words='english',
                                         use_idf=True, smooth_idf=True, sublinear_tf=False, encoding='utf-8',
                                         tokenizer=self.__tokenizer__ if self.__filter__ else None)

        train_data['data'] = self.__tfidf__.fit_transform(train_data['data'], train_data['targets'])
        print('Размер полученной tfidf матрицы: ', train_data['data'].shape)
        print(self.__tfidf__.vocabulary_)
        print(self.__filter__)


        # подбираем наилучшие параметры для классификатора
        # print('Подбор оптимальных параметров классификатора...')


        # обучаем классификатор SVM
        print('Обучение классификатора...')
        self.__predictor__ = SVC(kernel='linear', C=10, probability=True, random_state=0,
                                 decision_function_shape='ovo')
        # self.__predictor__ = SGDClassifier(loss='hinge', penalty='l2', alpha=1e-3,
        #                                    n_iter=5, random_state=42, probability=True)
        self.__predictor__.fit(train_data['data'], train_data['targets'])

        print('Создание модели успешно завершено.')

    def __call__(self, text=''):
        return self.get_topic(text)

    def get_topic(self, text=''):
        """
        предсказание темы переданного текста
        :param text:
        :return: None или {'main_topic': {'topic_name': score}, 'possible_topics': {'topic1': score, ...}}
        """
        text = self.__tfidf__.transform([text])  # преобразуем текст в вектор

        # if text is None:
        #     return None

        if hasattr(self.__predictor__, 'predict_proba'):
            # классификация с учетом вероятностей
            predicted = self.__predictor__.predict_proba(text)  # получаем массив вероятностей для классов
            predicted = predicted.tolist()[0]  # список вероятностей, отсортированный в соответствии с self.classes_
            # готовим словарь для отправки
            sorted_proba = sorted(zip(predicted, self.__target_names__), reverse=True)
            result = {'main_topic': self.__target_names__[predicted.index(max(predicted))],
                      'possible_topics': dict(sorted_proba[:3])}
        else:
            # классификация без учета вероятностей
            predicted = self.__predictor__.predict(text)[0]  # поулчаем псевдоним для класса
            predicted = self.__target_names__[predicted]  # получаем имя класса
            # готовим словарь для отправки
            result = {'main_topic': predicted, 'possible_topics': {predicted: 1}}
        return result

    def __tokenizer__(self, text):
        """
        Преобразует исходный текст в массив токенов/лемм,
        исключая из него не значимые для определения тематики значения.

        :param text: текст для разбора
        :return: [token1, token2, ...]
        """
        # ПРОВЕРКА МИНИМАЛЬНОЙ ДЛИНЫ ТЕКСТА
        if 'min_length' in self.__filter__:
            if len(text) < self.__filter__['min_length']:
                return []
        # print(1, text)

        # ЗАМЕНА/УДАЛЕНИЕ ЭЛЕМЕНТОВ В ТЕКСТЕ
        if 'replace_re' in self.__filter__:
            for replace in self.__filter__['replace_re']:
                text = re.sub(replace['target'], replace['substitute'], text)
        # print(2, text)

        # РАЗДЕЛЕНИЕ ТЕКСТА НА ТОКЕНЫ
        tmp_divider = self.__filter__['word_looks_re'] \
            if 'word_looks_re' in self.__filter__ else r"(?u)\b\w\w+\b"
        tokens = re.findall(tmp_divider, text)
        # приведение токенов к единому виду
        tokens = [x.lower() for x in tokens]
        # print(3, tokens)

        # ОКОНЧАТЕЛЬНАЯ ЧИСТКА
        if 'stop_words_re' in self.__filter__:
            for stop_word in self.__filter__['stop_words_re']:
                tokens = [x for x in tokens if not re.match(stop_word, x)]
        # print(4, tokens)

        # ПРОВЕРКА НА КОЛИЧЕСТВО ТОКЕНОВ
        if 'min_tokens' in self.__filter__:
            if len(tokens) < self.__filter__['min_tokens']:
                return []

        # ЛЕММАТИЗАЦИЯ
        if 'lemmatization' in self.__filter__:
            stemmer = Mystem()
            tokens = [stemmer.lemmatize(token)[0] for token in tokens]
        # print(5, tokens)

        return tokens

    def evaluate(self, file=None, data=None, **details):
        """
        оценка точности модели на указанных данных
        :param data:
        :return:
        """
    #     # оценка точности модели
    #     from sklearn import metrics
    #     print(
    #         cross_val_score(classificator, tfidf.transform(datasets('qwerty').data), datasets('qwerty').targets, cv=3))
    #
    #     predicted_dataset = classificator.predict(test_data)
    #     print(metrics.classification_report(t_targets, predicted_dataset,
    #                                         target_names=datasets('qwerty').target_names))
    #
    #     # получаем ошибки
    #     print(metrics.confusion_matrix(t_targets, predicted_dataset))
    #     print(datasets('qwerty').target_names)
    #
    # def cross_validation(self):
    #     """
    #     Кросс-валидация модели с указанными параметрами и
    #     :return:
    #     """
    #     pass


class Models:
    __models__ = {}  # объекты моделей

    def __call__(self, id):
        """
        при вызове models('id') возвращает модель с указанным id, если такая существует
        """
        return self.__models__.get(id)

    def create(self, id=DEFAULT_ID, data='', file='', filter_rules='', **details):
        """
        Создает новую модель с указанными параметрами
        :param id: id, под которым создается и сохраняется модель
        :param data: объект или словарь с данными, которые можно использовать для обучения
                     data.data, .target, .target_names, или data['data'], ['target'], ['target_names']
        :param file: путь к файлу, из которого необходимо считать образцы для обучения
        :param filter_rules: путь к файлу, из которого необходимо считать параметры фильтрации текста
        :param details: дополнительные параметры, которые используются для конфигурации загрузки и обучения модели
        :return:
        """
        # проверяем, есть ли загруженная модель, если загружена - дропаем
        if id in self.__models__:
            self.drop(id)

        # проверяем параметры создания модели
        buildargs = {}
        # проверяем наличие данных для обучения модели
        if data:
            buildargs['data'] = data
        else:
            file += '' if file else id + '.topic.csv'
            if os.path.exists(file):
                buildargs['file'] = file
            else:
                print('Невозможно создать модель без данных! '
                      'Укажите верно один из аргументов file или data.')
                return

        # проверка наличия фильтра
        filter_rules += '' if filter else id + '.topic.filter'
        if os.path.exists(filter_rules):
            buildargs['filter_rules'] = filter_rules
        else:
            print('Не удалось найти описание фильтрации для указанной модели, будет использоваться стандартная. '
                  'Для увеличения точности определения тематики рекомендуется использовать '
                  'подходящую для выбранной области фильтрацию.')

        # создаем новую модель
        self.__models__[id] = Model(**buildargs, **details)
        # сохраняем модель в файл с именем %ID%.topic
        with open(MODELS_PATH + id + '.topic', 'wb') as model_file:
            pickle.dump(self.__models__[id], model_file, pickle.HIGHEST_PROTOCOL)

    def load(self, id=DEFAULT_ID, file=''):
        """
        загрузить существующую модель
        """
        # проверяем, существует ли доступная для загрузки модель
        path = file if file else MODELS_PATH + id + '.topic'
        if os.path.exists(path):
            # проверяем, есть ли уже загруженная модель с таким id, если загружена - дропаем
            if id in self.__models__:
                self.drop(id)
            # загружаем модель из файла
            with open(path, 'rb') as model_file:
                self.__models__[id] = pickle.load(model_file)
            print('Модель ' + ('из ' + file if file else id) + ' успешно загружена.')
        else:
            print('Указанного файла модели не существует, проверьте правильность указанного пути.')

    def drop(self, id=''):
        """
        Выгрузить из памяти загруженную модель
        """
        self.__models__.pop(id, None)

    def delete(self, id=''):  # удалить файлы модели и выгрузить, если загружена
        pass

    def list(self, loaded=True, available=False):  # вывести список загруженных и доступных для загрузки моделей
        # class Result(list):  # для красивого вывода списка
        #     def __str__(self):
        #         return ', '.join(self)
        # result = Result()
        result = []
        if loaded:
            result.extend(list(self.__models__.keys()))
        if available:
            result.extend([fn for fn in os.listdir(MODELS_PATH) if fn.endswith('.topic')])
        return result

    def cross_validate(self):
        """
        Оценка обобщающей способности алгоритма с заданными параметрами (выводит результат, модель не сохраняется)
        """
        pass


models = Models()  # управляющий объект для моделей всегда один
