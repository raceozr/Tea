# -*- coding: utf-8 -*-
from teano import topic_analyser
from teano import data_loader
from teano import web_server
from unittest import TestCase


print(dir())

# Проверка модуля data_loader


# Проверка модуля topic_analyser
# создание модели
# topic_analyser.models.create('qwerty', file='./user_files/эквайринг_опс.csv',
#                              filter_rules='./user_files/ors_support.filter',
#                              class_col=2, text_col=1)

# topic_analyser.models.create('qwerty2', file='./user_files/ors_support.csv',
#                              filter_rules='./user_files/ors_support.filter',
#                              class_col=2, text_col=1)
# загрузка модели
topic_analyser.models.load('qwerty')
topic_analyser.models.load('qwerty2')

# проверка модели
print(topic_analyser.models.list())
print(topic_analyser.models('qwerty').get_topic('мама мыла раму, но не для того, чтобы об этом всем говорить'))
print(topic_analyser.models('qwerty2').get_topic('мама мыла раму, но не для того, чтобы об этом всем говорить'))
print(topic_analyser.models('qwerty').get_topic(''))
print(topic_analyser.models('qwerty2').get_topic('мама'))

# оценка модели


