# -*- coding: utf-8 -*-
from setuptools import setup
from os.path import join, dirname


setup(name='teano',
      version='0.1',
      description='Easy and flexible multitool for text analysis',
      long_description=open('README.md').read(),
      author='Lyubarsky Sergey Nikolaevich',
      author_email='S.N.Lyubarsky@ya.ru',
      url='https://bitbucket.org/RaceOZR/text_analyser/',
      packages=['teano'],
      install_requires=['pymystem3', 'sklearn', 'numpy', 'scipy'],
      zip_safe=False,
      entry_points={
          'console_scripts': ['start_server = teano.web_server:start']
        }
      )
